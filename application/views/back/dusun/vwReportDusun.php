<?php $this->load->view('back/layout/vwHeadReport') ?>
<?php $no = 1;
	if($data_dusun->num_rows() > 0) { ?>
		<br/>
		<p style="text-align:center">Cetak Dusun</p>
	
		
		<table align="center">
			<tr>
				<th style='text-align:center;' rowspan="2">No</th>
				<th style='text-align:center' rowspan="2" >Nama Dusun</th>
				<th style='text-align:center' rowspan="2" >Ketua Dusun</th>
				<th style='text-align:center' rowspan="2" >Alamat Dusun</th>
				<th style='text-align:center' colspan="4">Jumlah</th>
			</tr>
			<tr>
				<th style='width:7%;text-align:center'>RW</th>
				<th style='width:7%;text-align:center'>RT</th>
				<th style='width:7%;text-align:center'>KK</th>
				<th style='width:9%;text-align:center'>INDIVIDU</th>
			</tr>
			<?php foreach($data_dusun->result() as $row) { ?>
			<tr >
				<td align="center"><?php echo $no;?></td>
				<td><?php echo $row->NAMA_DSN; ?></td>
				<td><?php echo $row->KETUA_DSN; ?></td>
				<td><?php echo $row->ALAMAT_DSN; ?></td>
				<td align="center"><?php echo $rw = get_count_rw(array('ID_DUSUN' => $row->ID))?></td>
				<td align="center"><?php echo $rt = get_count_rt(array('ID_DUSUN' => $row->ID))?></td>
				<td align="center"><?php echo $kk = get_count_kk(array('ID_DUSUN' => $row->ID))?></td>
				<td align="center"><?php echo $in = get_count_individu(array('ID_DUSUN' => $row->ID))?></td>
			</tr>
			<?php $no++; } ?>
		</table>

	<?php } else {
	echo "<br/><center><i class='fa fa-exclamation-triangle fa-3x' style='margin-top:10px;tex-align:center'></i> <br/>Data tidak ditemukan</center><br/>";
}?>

</body>
</html>
