<section class="content-header">
    <h1>
        Dusun
        <small>Manajemen Dusun</small>
    </h1>
</section>
<br/>
<ol class="breadcrumb">
    <li><a href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> &nbsp;Dashboard</a></li>
    <li class="active">List Dusun</li>
</ol>
<section class="content">
     <div class='row' style='margin-top:-20px;margin-bottom:10px'>
        <div class='col-sm-5 col-xs-12' style='margin-top:5px;margin-bottom:5px'>
            <button class='btn btn-success col-xs-12 col-md-3 btn-sm' data-target="#tambah-dusun" data-toggle="modal" ><i class='fa fa-plus'></i> Tambah Dusun</button>
            <br/>
        </div>
        <div class='col-sm-4 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
			<div class="input-group pull-right">
				<input type="text" name="cari" id='cari' class="form-control input-sm col-sm-4 col-xs-12" placeholder="Cari Dusun . . ." onchange='pageLoad(1)'>
				<div class="input-group-btn">
					<button class="btn btn-default btn-sm"><i class="fa fa-search" style="border-radius:0 3px 3px 0"></i></button>
					<a class="btn btn-primary btn-sm" target="_blank" href="<?php echo site_url('report/cetak_dusun')?>" style="margin-left:10px;border-radius:3px"><i class="fa fa-print"></i> Cetak</a>
				</div>
			</div>
        </div>
		<div class='col-sm-2 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
            <select name='limit' id='limit' class="form-control input-sm col-sm-4 col-xs-12" onchange='pageLoad(1)'>
                <option value='5' >5 rows</option>
                <option value='10' >10 rows</option>
                <option value='25' >25 rows</option>
            </select>
        </div>
		
    </div>
	
    <div id='dataDusun'>
		<div class='row' id='loading' style='display:none'>
			<div class='col-md-12'>
				<div class="box box-primary">
					<div class="box-header">
						
					</div>
					<div class="box-body">
					 
					</div>
					
					<div class="overlay">

						<i class="fa fa-spinner fa-pulse fa-4x"></i>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal tambah dusun !-->
<div id='tambah-dusun' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Tambah Dusun</h4>
			</div>
			<?php echo form_open('dusun/tambah_dusun')?>
			<div class="modal-body" style="min-height:300px;">
				<div class="col-md-12">
					<div class="form-group" id="form-nama" >
						<label for="nama_simpan">Nama Dusun</label>
						<input type="text" class="form-control" id="nama_simpan" name='nama_simpan' placeholder='Masukan Nama Dusun..' required onchange="cekNamaDusun(this.value)" >
						<label for="nama_simpan" id="alert-nama" style="display:none;" ><i>Nama dusun sudah ada</i></label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="ketua_simpan">Nama Ketua Dusun</label>
						<input type="text" class="form-control" id="ketua_simpan" name='ketua_simpan' placeholder='Masukan Nama Ketua Dusun..'>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="alamat_simpan">Alamat Dusun</label>
						<textarea type="text" class="form-control" id="alamat_simpan" name='alamat_simpan' placeholder='Masukan Alamat Dusun..' ></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary" name='simpan_dusun' id="simpan_dusun" ><i class='fa fa-check'></i> Simpan</button>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>

<!-- modal edit dusun !-->
<div id='edit-dusun' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Edit Dusun</h4>
			</div>
			<div class="modal-body" style="min-height:300px;">
				<div class="col-md-12">
					<div class="form-group"id="form-nama-edit" >
						<label for="nama_edit">Nama Dusun</label>
						<input type="text" class="form-control" id="nama_edit" name='nama_edit' placeholder='Masukan Nama Dusun..' required >
						<label for="nama_edit" id="alert-nama-edit" style="display:none;" ><i>Nama dusun tidak boleh kosong</i></label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="ketua_edit">Nama Ketua Dusun</label>
						<input type="text" class="form-control" id="ketua_edit" name='ketua_edit' placeholder='Masukan Nama Ketua Dusun..'>
						<input type="hidden" class="form-control" id="id_edit" name='id_edit'  required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="alamat_edit">Alamat Dusun</label>
						<textarea type="text" class="form-control" id="alamat_edit" name='alamat_edit' placeholder='Masukan Alamat Dusun..' ></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"  id="cancel" ><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary" name='edit_dusun' id="edit_dusun"  onclick="editDataDusun()" ><i class='fa fa-edit'></i> Edit</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	pageLoad(1);
	
	<?php if($this->session->flashdata('hasil') == 2) {?>
		alertify.error('<?php echo $this->session->flashdata('msg')?>');
	<?php } else if($this->session->flashdata('hasil') == 1) { ?>
		alertify.success('<?php echo $this->session->flashdata('msg')?>');
	<?php } else {} ?>
	
	$('[data-toggle="tooltip"]').tooltip();
	
	alertify.set({ labels: {
		ok     : "Ya",
		cancel : "Tidak"
		 
		} 
	});

	$('#cancel').click(function(){
		$('#form-nama-edit').removeClass('has-error');
		$('#alert-nama-edit').attr('style', 'display:none');
	});
});

function pageLoad(i){
	var limit 	= $('#limit').val();
	var cari 	= $('#cari').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>dusun/read/'+i,
		type	: 'post',
		dataType: 'html',
		data	: {limit:limit,cari:cari},
		beforeSend : function(){
			$('#loading').fadeIn('slow');
		},
		success : function(result){
			$('#loading').attr('style','display:none');
			$('#dataDusun').html(result);
		}
	})
}

function cekNamaDusun(x){
	if(x != ''){
		$.ajax({
			url		: '<?php echo site_url()?>dusun/cek_nama_dusun',
			type	: 'post',
			dataType: 'json',
			data	: {x:x},
			beforeSend : function(){

			},
			success : function(result){
				if(result.rs == 1){
					$('#form-nama').removeClass('has-error');
					$('#alert-nama').attr('style', 'display:none');
				}else{
					$('#form-nama').addClass('has-error');
					$('#alert-nama').attr('style', 'display:inline');
				}
			}
		});
	}else{
		$('#form-nama').removeClass('has-error');
		$('#alert-nama').attr('style', 'display:none');
	}
}

function getDataDusunForEdit(i,x,y,z){
	$('#nama_edit').val(x);
	$('#ketua_edit').val(y);
	$('#alamat_edit').val(z);
	$('#id_edit').val(i);
}

function deleteDusun(x){
	alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
		if (e) {
			$.ajax({
				url		: '<?php echo site_url()?>dusun/delete_dusun',
				type	: 'post',
				dataType: 'json',
				data	: {x:x},
				beforeSend : function(){

				},
				success : function(result){
					if(result.rs == 1){
						pageLoad($('#current').val());
						alertify.success("<b> Data berhasil dihapus</b>");
					}else{
						alertify.error("<b>Data berhasil dihapus</b>");
					}
				}
			});
		}
	});
}

function editDataDusun(){
	var nama	= $('#nama_edit').val();
	var	ketua 	= $('#ketua_edit').val();
	var alamat	= $('#alamat_edit').val();
	var id		= $('#id_edit').val();
	
	if(nama != ''){
		$.ajax({
			url		: '<?php echo site_url()?>dusun/edit_dusun',
			type	: 'post',
			dataType: 'json',
			data	: {nama:nama,ketua:ketua,alamat:alamat,id:id},
			beforeSend : function(){

			},
			success : function(result){
				if(result.rs == 1){
					$('#cancel').click();
					pageLoad($('#current').val());
					alertify.success(result.msg);
				}else{
					alertify.error(result.msg);
				}
			}
		});
	}else{
		$('#form-nama-edit').addClass('has-error');
		$('#alert-nama-edit').attr('style', 'display:inline');
		
	}
}

</script>