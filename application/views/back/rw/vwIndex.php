<section class="content-header">
    <h1>
        RW
        <small>Manajemen RW</small>
    </h1>
</section>
<br/>
<ol class="breadcrumb">
    <li><a href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> &nbsp;Dashboard</a></li>
    <li class="active">List RW</li>
</ol>
<section class="content">
     <div class='row' style='margin-top:-20px;margin-bottom:10px'>
        <div class='col-sm-5 col-xs-12' style='margin-top:5px;margin-bottom:5px'>
            <button class='btn btn-success col-xs-12 col-md-3 btn-sm' data-target="#tambah-rw" data-toggle="modal" ><i class='fa fa-plus'></i> Tambah RW</button>
            <br/>
        </div>
        <div class='col-sm-4 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
			<div class="input-group pull-right">
				<input type="text" name="cari" id='cari' class="form-control input-sm col-sm-4 col-xs-12" placeholder="Cari RW . . ." onchange='pageLoad(1)'>
				<div class="input-group-btn">
					<button class="btn btn-default btn-sm"><i class="fa fa-search" style="border-radius:0 3px 3px 0"></i></button>
					<a class="btn btn-primary btn-sm" href="<?php echo site_url('report/cetak_rw')?>" target='_blank'style="margin-left:10px;border-radius:3px"><i class="fa fa-print"></i> Cetak</a>
				</div>
			</div>
        </div>
		<div class='col-sm-2 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
            <select name='limit' id='limit' class="form-control input-sm col-sm-4 col-xs-12" onchange='pageLoad(1)'>
                <option value='5' >5 rows</option>
                <option value='10' >10 rows</option>
                <option value='25' >25 rows</option>
            </select>
        </div>
		
    </div>
	
    <div id='dataDusun'>
		<div class='row' id='loading' style='display:none'>
			<div class='col-md-12'>
				<div class="box box-primary">
					<div class="box-header">
						
					</div>
					<div class="box-body">
					 
					</div>
					
					<div class="overlay">

						<i class="fa fa-spinner fa-pulse fa-4x"></i>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal tambah rw !-->
<div id='tambah-rw' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Tambah RW</h4>
			</div>
			<?php echo form_open('rw/tambah_rw')?>
			<div class="modal-body" style="min-height:100px;">
				<div class="col-md-12">
					<div class="form-group">
						<label for="dusun_simpan">Pilih Dusun</label>
						<select class="form-control" id="dusun_simpan" name='dusun_simpan' placeholder='Masukan Nama RW..' required onchange="cekNomorRw()" >
							<option value="" >-- PILIH DUSUN --</option>
							<?php echo get_select_dusun(); ?>
						</select>
					</div>
				</div>
				<div id="show-tambah-rw">
					<div class="col-md-12">
						<div class="form-group" id="form-nomor" >
							<label for="nomor_simpan">Nomor RW</label>
							<input type="text" class="form-control" id="nomor_simpan" name='nomor_simpan' placeholder='Masukan Nomor RW..' required onchange="cekNomorRw()" >
							<label for="nomor_simpan" id="alert-nomor" style="display:none;" ><i>Nomor rw pada dusun yang anda pilih sudah ada</i></label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="ketua_simpan">Nama Ketua RW</label>
							<input type="text" class="form-control" id="ketua_simpan" name='ketua_simpan' placeholder='Masukan Nama Ketua RW..'>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="alamat_simpan">Alamat RW</label>
							<textarea type="text" class="form-control" id="alamat_simpan" name='alamat_simpan' placeholder='Masukan Alamat RW..' ></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="cancel_tambah"><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary" name='simpan_rw' id="simpan_rw" disabled ><i class='fa fa-check'></i> Simpan</button>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>

<!-- modal edit rw !-->
<div id='edit-rw' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Edit RW</h4>
			</div>
			<div class="modal-body" style="min-height:300px;">
				<div class="col-md-12">
					<div class="form-group">
						<label for="dusun_edit">Pilih Dusun</label>
						<select class="form-control" id="dusun_edit" name='dusun_edit' placeholder='Masukan Nama RW..' required onchange="cekNomorRwForEdit()" >
							<option value="" >-- PILIH DUSUN --</option>
							<?php echo get_select_dusun() ?>
						</select>
					</div>
				</div>
				<div id="show-edit-rw">
					<div class="col-md-12">
						<div class="form-group"id="form-nomor2" >
							<label for="nomor_edit">Nomor RW</label>
							<input type="text" class="form-control" id="nomor_edit" name='nomor_edit' placeholder='Masukan Nomor RW..' required onchange="cekNomorRwForEdit()">
							<label for="nomor_edit" id="alert-nomor2" style="display:none;" ><i>Nomor RW pada dusun yang anda pilih sudah ada</i></label>
							<label for="nomor_edit" id="alert-edit" style="display:none;" ><i>Dusun dan Nomor RW tidak boleh kosong</i></label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="ketua_edit">Nama Ketua RW</label>
							<input type="text" class="form-control" id="ketua_edit" name='ketua_edit' placeholder='Masukan Nama Ketua RW..'>
							<input type="hidden" class="form-control" id="id_edit" name='id_edit'  required>
							<input type="hidden" class="form-control" id="dusun_hide" name='dusun_hide'  required>
							<input type="hidden" class="form-control" id="nomor_hide" name='nomor_hide'  required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="alamat_edit">Alamat RW</label>
							<textarea type="text" class="form-control" id="alamat_edit" name='alamat_edit' placeholder='Masukan Alamat RW..' ></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"  id="cancel_edit" ><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary" name='edit_rw' id="edit_rw" onclick="editDataRw()" ><i class='fa fa-edit'></i> Edit</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	pageLoad(1);
	
	<?php if($this->session->flashdata('hasil') == 2) {?>
		alertify.error('<?php echo $this->session->flashdata('msg')?>');
	<?php } else if($this->session->flashdata('hasil') == 1) { ?>
		alertify.success('<?php echo $this->session->flashdata('msg')?>');
	<?php } else {} ?>
	
	$('[data-toggle="tooltip"]').tooltip();
	
	alertify.set({ labels: {
		ok     : "Ya",
		cancel : "Tidak"
		 
		} 
	});

	$('#cancel_tambah').click(function(){
		$('#dusun_simpan').val('');
		$('#nomor_simpan').val('');
		$('#ketua_simpan').val('');
		$('#alamat_simpan').val('');
		$('#form-nomor').removeClass('has-error');
		$('#alert-nomor').hide('slow');
	});
	$('#cancel_edit').click(function(){
		$('#dusun_edit').val('');
		$('#nomor_edit').val('');
		$('#ketua_edit').val('');
		$('#alamat_edit').val('');
		$('#form-nomor2').removeClass('has-error');
		$('#alert-nomor2').fadeOut('slow');
	});
});

function pageLoad(i){
	var limit 	= $('#limit').val();
	var cari 	= $('#cari').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>rw/read/'+i,
		type	: 'post',
		dataType: 'html',
		data	: {limit:limit,cari:cari},
		beforeSend : function(){
			$('#loading').fadeIn('slow');
		},
		success : function(result){
			$('#loading').attr('style','display:none');
			$('#dataDusun').html(result);
		}
	})
}

/*function showTambahRw(x){
	if(x != ''){
		$('#show-tambah-rw').fadeIn('slow');
		$('#show-edit-rw').fadeIn('slow');
	}else{
		$('#show-edit-rw').fadeOut('slow');

	}
}*/

function cekNomorRw(){
	var dusun	= $('#dusun_simpan').val();
	var rw		= $('#nomor_simpan').val();
	if(rw != '' && dusun != '' ){
		$.ajax({
			url		: '<?php echo site_url()?>rw/cek_nomor_rw',
			type	: 'post',
			dataType: 'json',
			data	: {rw:rw,dusun:dusun},
			beforeSend : function(){

			},
			success : function(result){
				if(result.rs == 1){
					$('#form-nomor').removeClass('has-error');
					$('#alert-nomor').attr('style', 'display:none');
					$( "#simpan_rw" ).prop( "disabled", false );
				}else{
					$('#form-nomor').addClass('has-error');
					$('#alert-nomor').attr('style', 'display:inline');
					$( "#simpan_rw" ).prop( "disabled", true );
				}
			}
		});
	}else{
		$('#form-nomor').removeClass('has-error');
		$('#alert-nomor').attr('style', 'display:none');
		$( "#simpan_rw" ).prop( "disabled", true );
	}
}

function cekNomorRwForEdit(){
	var dusun	= $('#dusun_edit').val();
	var rw		= $('#nomor_edit').val();
	var dusun_h = $('#dusun_hide').val();
	var rw_h	= $('#nomor_hide').val();

	if(rw != '' && dusun != ''){
		if(dusun == dusun_h && rw == rw_h){
			$('#form-nomor2').removeClass('has-error');
			$('#alert-nomor2').attr('style', 'display:none');
			$( "#edit_rw" ).prop( "disabled", false );
		}else{
		
			$.ajax({
				url		: '<?php echo site_url()?>rw/cek_nomor_rw',
				type	: 'post',
				dataType: 'json',
				data	: {rw:rw,dusun:dusun},
				beforeSend : function(){

				},
				success : function(result){
					if(result.rs == 1){
						$('#form-nomor2').removeClass('has-error');
						$('#alert-nomor2').attr('style', 'display:none');
						$( "#edit_rw" ).prop( "disabled", false );
					}else{
						$('#form-nomor2').addClass('has-error');
						$('#alert-nomor2').attr('style', 'display:inline');
						$( "#edit_rw" ).prop( "disabled", true );
					}
				}
			});	
		}
	}else{
		$('#form-nomor2').removeClass('has-error');
		$('#alert-nomor2').attr('style', 'display:none');
		$("#edit_rw").prop( "disabled", true );
	}
}

function getDataRwForEdit(i,w,x,y,z){
	$('#dusun_edit').val(w);
	$('#dusun_hide').val(w);
	$('#nomor_edit').val(x);
	$('#nomor_hide').val(x);
	$('#ketua_edit').val(y);
	$('#alamat_edit').val(z);
	$('#id_edit').val(i);
}

function deleteRw(x){
	alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
		if (e) {
			$.ajax({
				url		: '<?php echo site_url()?>rw/delete_rw',
				type	: 'post',
				dataType: 'json',
				data	: {x:x},
				beforeSend : function(){

				},
				success : function(result){
					if(result.rs == 1){
						pageLoad($('#current').val());
						alertify.success("<b> Data berhasil dihapus</b>");
					}else{
						alertify.error("<b>Data berhasil dihapus</b>");
					}
				}
			});
		}
	});
}

function editDataRw(){
	var dusun	= $('#dusun_edit').val();
	var nomor	= $('#nomor_edit').val();
	var	ketua 	= $('#ketua_edit').val();
	var alamat	= $('#alamat_edit').val();
	var id		= $('#id_edit').val();
	
	if(nomor != '' && dusun !='' ){
		$.ajax({
			url		: '<?php echo site_url()?>rw/edit_rw',
			type	: 'post',
			dataType: 'json',
			data	: {nomor:nomor,ketua:ketua,alamat:alamat,id:id,dusun:dusun},
			beforeSend : function(){

			},
			success : function(result){
				if(result.rs == 1){
					$('#cancel_edit').click();
					pageLoad($('#current').val());
					alertify.success(result.msg);
				}else{
					alertify.error(result.msg);
				}
			}
		});
	}else{
		$('#form-nomor2').addClass('has-error');
		$('#alert-edit').attr('style', 'display:inline');
		
	}
}

</script>