<?php $this->load->view('back/layout/vwHeadReport') ?>

<?php $no = 1;
	if($data->num_rows() > 0) { ?>
	<br/>
	<p style="text-align:center">Cetak Dusun</p>
		
	<table align='center'>
		<tr>
			<th style='width:5%;text-align:center;' rowspan="2">No</th>
			<th style='width:15%;text-align:center' rowspan="2" >Nomor RW</th>
			<th style='width:20%;text-align:center' rowspan="2" >Alamat RW</th>
			<th style='width:30%;text-align:center' colspan="3">Jumlah</th>
		</tr>
		<tr>
			<th style='width:7%;text-align:center'>RT</th>
			<th style='width:7%;text-align:center'>KK</th>
			<th style='width:9%;text-align:center'>INDIVIDU</th>
		</tr>
		<?php foreach($data->result() as $row) { ?>
		<tr>
			<td align="center"><?php echo $no;?></td>
			<td><?php echo $row->NOMOR_RW; ?></td>
			<td><?php echo $row->ALAMAT_RW; ?></td>
			<td align="center"><?php echo $rt = get_count_rt(array('ID_RW' => $row->ID))?></td>
			<td align="center"><?php echo $kk = get_count_kk(array('ID_RW' => $row->ID))?></td>
			<td align="center"><?php echo $in = get_count_individu(array('ID_RW' => $row->ID))?></td>
		</tr>
		<?php $no++; } ?>
	</table>
	<?php } else {
	echo "<br/><center><i class='fa fa-exclamation-triangle fa-3x' style='margin-top:10px;'></i> <br/>Tidak ada nomor rw dg kata kunci <b>$cari</b></center><br/>";
}?>

