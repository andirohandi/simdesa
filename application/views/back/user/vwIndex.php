<section class="content-header">
    <h1>
        User
        <small>Manajemen User</small>
    </h1>
</section>
<br/>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> &nbsp;Dashboard</a></li>
    <li class="active">List User</li>
</ol>
<section class="content">
     <div class='row' style='margin-top:-20px;margin-bottom:10px'>
        <div class='col-sm-5 col-xs-12' style='margin-top:5px;margin-bottom:5px'>
            <button class='btn btn-success col-xs-12 col-md-3 btn-sm' data-target="#tambah-user" data-toggle="modal" ><i class='fa fa-plus'></i> Tambah User</button>
            <br/>
        </div>
        <div class='col-sm-3 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
            <div class="input-group pull-right">
                <input type="text" name="cari" id='cari' class="form-control input-sm col-sm-4 col-xs-12" placeholder="Cari User . . ." onchange='pageLoad(1)'>
                <div class="input-group-btn">
                    <button class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
		 <div class='col-sm-2 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:5px'>
            <select name='limit' id='limit' class="form-control input-sm col-sm-4 col-xs-12" onchange='pageLoad(1)'>
                <option value='5' >5 rows</option>
                <option value='10' >10 rows</option>
                <option value='25' >25 rows</option>
            </select>
        </div>
    </div>
	
    <div id='dataUser'>
		<div class='row' id='loading' style='display:none'>
			<div class='col-md-12'>
				<div class="box box-primary">
					<div class="box-header">
						
					</div>
					<div class="box-body">
					 
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal tambah user !-->
<div id='tambah-user' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Tambah User</h4>
			</div>
			<?php echo form_open('user/tambah_user')?>
			<div class="modal-body" style="min-height:400px;">
				<div id='msg-tambah-user' class='alert alert-danger msg-tambah-user'></div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="nama_simpan">Nama</label>
						<input type="text" class="form-control" id="nama_simpan" name='nama_simpan' placeholder='Masukan Nama..' required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="username_simpan">Username</label>
						<input type="text" class="form-control" id="username_simpan" name='username_simpan' placeholder='Masukan Username..' required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="password_baru_simpan">Password</label>
						<input type="password" class="form-control" id="password_baru_simpan" name='password_baru_simpan' placeholder='Masukan Password..' required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="password_ulang_simpan">Konfirmasi Password</label>
						<input type="password" class="form-control" id="password_ulang_simpan" name='password_ulang_simpan' placeholder='Masukan Ulang Password..' required >
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="kategori_simpan">Kategori User</label>
						<select class="form-control" id="kategori_simpan" name='kategori_simpan' required >
							<option value="" >-- Pilih Kategori --</option>
							<option value="1" >ADMINISTRATOR</option>
							<option value="0" >USER</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-primary pull-right" onclick='check_pass_simpan()'><i class='fa fa-check'></i> Simpan</button>
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary hide" name='simpan_user' id="simpan_user" ><i class='fa fa-check'></i> Ya</button>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>

<!-- modal edit user !-->
<div id='edit-user' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class='fa fa-gear'></i> Form Edit User</h4>
			</div>
			<?php echo form_open('user/edit_user')?>
			<div class="modal-body" style="min-height:250px;">
				<div class="col-md-12">
					<div class="form-group">
						<label for="nama_edit">Nama</label>
						<input type="text" class="form-control" id="nama_edit" name='nama_edit' placeholder='Masukan Nama..' required >
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="username_edit">Username</label>
						<input type="text" class="form-control" id="username_edit" name='username_edit' placeholder='Masukan Username..' required>
						<input type="hidden" class="form-control" id="id_edit" name='id_edit' required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="kategori_edit">Kategori User</label>
						<select class="form-control" id="kategori_edit" name='kategori_edit' required >
							<option value="" >-- Pilih Kategori --</option>
							<option value="1" >ADMINISTRATOR</option>
							<option value="0" >USER</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-primary" name='edit_user' id="edit_user" ><i class='fa fa-edit'></i> Edit</button>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>

<!-- modal delete user !-->
<div id='delete-user' class='modal custom fade' tabindex='-1' role='dialog'aria-hidden='true' data-backdrop='static'>
	<div class="modal-dialog">
		<div class="modal-content modal-info">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class='fa fa-exclamation-circle'></i> Konfirmasi</h4>
			</div>
			<?php echo form_open('user/delete_user')?>
			<div class="modal-body" style="min-height:50px;">
				<div class="col-md-12">
					<div class="form-group">
						Apakah anda yakin akan menghapus data ini ?
						<input type="hidden" class="form-control" id="id_delete" name='id_delete' required >
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="button" class="btn btn-info pull-left" data-dismiss="modal"><i class='fa fa-remove'></i> Cancel</button>
					<button type="submit" class="btn btn-info" name='delete_user' id="delete_user" ><i class='fa fa-check'></i> Ya</button>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	pageLoad(1);
	$('#msg-tambah-user').hide();
	
	<?php if($this->session->flashdata('hasil') == 2) {?>
		alertify.error('<?php echo $this->session->flashdata('msg')?>');
	<?php } else if($this->session->flashdata('hasil') == 1) { ?>
		alertify.success('<?php echo $this->session->flashdata('msg')?>');
	<?php } else {} ?>
});

function check_pass_simpan() {
	$('.msg-tambah-user').hide();
	if($('#password_baru_simpan').val() != $('#password_ulang_simpan').val()) {
		$('#msg-tambah-user').html("<i class='fa fa-remove'></i> Password Baru & Konfirmasi Password Harus Sesuai");
		$('#msg-tambah-user').show();
	} else {
		$('#simpan_user').click();
	}
}

function pageLoad(i){
	var limit 	= $('#limit').val();
	var status 	= $('#status').val();
	var cari 	= $('#cari').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>user/read/'+i,
		type	: 'post',
		dataType: 'html',
		data	: {limit:limit,cari:cari},
		beforeSend : function(){
			$('#loading').fadeIn('slow');
		},
		success : function(result){
			$('#loading').attr('style','display:none');
			$('#dataUser').html(result);
		}
	})
}

function getDataUserForEdit(w,x,y,z){
	$('#nama_edit').val(x);
	$('#id_edit').val(w);
	$('#username_edit').val(y);
	$('#kategori_edit').val(z);
}

function getIdUserForDelete(x){
	$('#id_delete').val(x);
}

</script>