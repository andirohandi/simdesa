<div class='row'>
	<div class='col-sm-12'>
		<div class="box box-success">
			<div class="box-body table-responsive no-padding">
				<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
					$no++;
					if($data_user->num_rows() > 0) { ?>
					<table class="table table-hover table-bordered">
						<tr>
							<th style='width:5%;text-align:center'>No</th>
							<th style='width:25%;text-align:center'>Nama</th>
							<th style='width:25%;text-align:center'>Username</th>
							<th style='width:15%;text-align:center'>Kategori</th>
							<th style='width:30%;text-align:center'>Action</th>
						</tr>
						<?php foreach($data_user->result() as $row) { 
							$kategori	= "";
							if($row->LEVEL == 1) $kategori = "ADMINISTRATOR";
							else $kategori = "USER";
						?>
						<tr>
							<td align="center"><?php echo $no;?></td>
							<td><?php echo $row->NAMA; ?></td>
							<td><?php echo $row->USERNAME; ?></td>
							<td align="center"><?php echo $kategori; ?></td>
							<td align="center">
								<button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#ganti-password" onclick="getIdUsergantiPassword('<?php echo $row->ID; ?>')" ><i class="fa fa-key"></i> Ganti Password</button>
								<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-user" onclick="getDataUserForEdit('<?php echo $row->ID; ?>', '<?php echo $row->NAMA; ?>', '<?php echo $row->USERNAME; ?>', '<?php echo $row->LEVEL; ?>' )" ><i class="fa fa-edit"></i> Edit</button>
								<button href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-user" onclick="getIdUserForDelete('<?php echo encode($row->ID); ?>')"  ><i class="fa fa-trash"></i> Hapus</button>
							</td>
						</tr>
						<?php $no++; } ?>
					</table>
					<?php } else {
					echo "<br/><center><i class='fa fa-exclamation-triangle fa-3x' style='margin-top:10px;'></i> <br/>Tidak ada nama ataupun username dengan kata kunci <b>$cari</b></center><br/>";
				}?>
				<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
			</div>
		</div>
	</div>
</div>
<div class='row'>
	<div class='col-sm-4 col-xs-12' style='margin-top:10px;margin-bottom:10px'>
		Total <?php echo $paging['count_row']?> Rows 
	</div>
	<div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
		<?php echo $paging['list'] ?>
	</div>
</div>