<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
</section>
<br/>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
</ol>
<section class="content">
    <div class='row'>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-gears"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">ROLES</span>
					<span class="info-box-number">90<small>%</small></span>
					<a href="<?php echo site_url('role')?>" class='btn btn-sm pull-right bg-aqua'><i class='fa fa-eye'></i> Detail</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
		  <div class="info-box">
			<span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">USER</span>
			  <span class="info-box-number">41,410</span>
			  <a href="<?php echo site_url('user')?>" class='btn btn-sm pull-right bg-red'><i class='fa fa-eye'></i> Detail</a>
			</div>
		  </div>
		</div>
		
		<div class="clearfix visible-sm-block"></div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Berita</span>
					<span class="info-box-number">760</span>
					<a href="<?php echo site_url('berita')?>" class='btn btn-sm pull-right bg-green'><i class='fa fa-eye'></i> Detail</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Info</span>
					<span class="info-box-number">2,000</span>
					<a href="<?php echo site_url('info')?>" class='btn btn-sm pull-right bg-yellow'><i class='fa fa-eye'></i> Detail</a>
				</div>
			</div>
		</div>
    </div>
</section>