
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
            <li class="<?php echo $pg == 'dashboard' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('dashboard') ?>">
                    <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
                </a>
            </li>
			<li class="treeview <?php echo $pg == 'profile' || $pg == 'aparator' || $pg == 'inventaris' ? 'active' : '' ?>">
				<a href="#">
					<i class="fa fa-home"></i> <span>DESA</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class='<?php echo $pg == 'profile' ? 'active' : '' ?>'><a href="<?php echo site_url('profile')?>"><i class="fa fa-angle-double-right"></i> Profile</a></li>
					<li class='<?php echo $pg == 'aparator' ? 'active' : '' ?>'><a href="<?php echo site_url('aparator')?>"><i class="fa fa-angle-double-right"></i> Aparator</a></li>
					<li class='<?php echo $pg == 'inventaris' ? 'active' : '' ?>'><a href="<?php echo site_url('inventaris')?>"><i class="fa fa-angle-double-right"></i> Inventaris</a></li>
				</ul>
			</li>
			<li class="treeview <?php echo $pg == 'dusun' || $pg == 'rw' || $pg == 'rt' || $pg == 'kk' || $pg == 'individu' ? 'active' : '' ?>">
				<a href="#">
					<i class="fa fa-users"></i> <span>PENDUDUK</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class='<?php echo $pg == 'dusun' ? 'active' : '' ?>'><a href="<?php echo site_url('dusun')?>"><i class="fa fa-angle-double-right"></i> Dusun</a></li>
					<li class='<?php echo $pg == 'rw' ? 'active' : '' ?>'><a href="<?php echo site_url('rw')?>"><i class="fa fa-angle-double-right"></i> RW</a></li>
					<li class='<?php echo $pg == 'rt' ? 'active' : '' ?>'><a href="<?php echo site_url('rt')?>"><i class="fa fa-angle-double-right"></i> RT</a></li>
					<li class='<?php echo $pg == 'kk' ? 'active' : '' ?>'><a href="<?php echo site_url('kk')?>"><i class="fa fa-angle-double-right"></i> KK</a></li>
					<li class='<?php echo $pg == 'individu' ? 'active' : '' ?>'><a href="<?php echo site_url('individu')?>"><i class="fa fa-angle-double-right"></i> Individu</a></li>
				</ul>
			</li>
			<li class="treeview <?php echo $pg == 'pendidikan' || $pg == 'sarana_ibadah' || $pg == 'waralaba'  ? 'active' : ''  ?>">
				<a href="#">
					<i class="fa fa-area-chart"></i> <span>INFRASTRUKTUR</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class='<?php echo $pg == 'pendidikan' ? 'active' : '' ?>'><a href="<?php echo site_url('pendidikan')?>"><i class="fa fa-angle-double-right"></i> Pendidikan</a></li>
					<li class='<?php echo $pg == 'sarana_ibadah' ? 'active' : '' ?>'><a href="<?php echo site_url('sarana_ibadah')?>"><i class="fa fa-angle-double-right"></i> Sarana Ibadah</a></li>
					<li class='<?php echo $pg == 'waralaba' ? 'active' : '' ?>'><a href="<?php echo site_url('waralaba')?>"><i class="fa fa-angle-double-right"></i> Waralaba</a></li>
				</ul>
			</li>
			<?php if($this->session->userdata('LEVEL') == 1) { ?>
			<li class="treeview <?php echo $pg == 'user' || $pg == 'backup_database' ? 'active' : ''  ?>">
				<a href="#">
					<i class="fa fa-gear"></i> <span>UTILITY</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class='<?php echo $pg == 'user' ? 'active' : '' ?>'><a href="<?php echo site_url('user')?>"><i class="fa fa-angle-double-right"></i> User</a></li>
					<li class='<?php echo $pg == 'backup_database' ? 'active' : '' ?>'><a href="<?php echo site_url('backup_database')?>"><i class="fa fa-angle-double-right"></i> Backup Database</a></li>
				</ul>
			</li>
			<?php } ?>
        </ul>
    </section>
</aside>

<div class="content-wrapper">