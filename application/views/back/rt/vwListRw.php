<div class='row'>
	<div class='col-sm-12'>
		<div class="box box-success">
			<div class="box-body table-responsive no-padding">
				<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
					$no++;
					if($data->num_rows() > 0) { ?>
					<table class="table table-hover table-bordered">
						<tr>
							<th style='width:5%;text-align:center;' rowspan="2">No</th>
							<th style='width:15%;text-align:center' rowspan="2" >Nomor RW</th>
							<th style='width:20%;text-align:center' rowspan="2" >Alamat RW</th>
							<th style='width:30%;text-align:center' colspan="2">Jumlah</th>
							<th style='width:30%;text-align:center' rowspan="2" >Action</th>
						</tr>
						<tr>
							<th style='width:7%;text-align:center'>KK</th>
							<th style='width:9%;text-align:center'>INDIVIDU</th>
						</tr>
						<?php foreach($data->result() as $row) { ?>
						<tr>
							<td align="center"><?php echo $no;?></td>
							<td><?php echo $row->NOMOR_RT; ?></td>
							<td><?php echo $row->ALAMAT_RT; ?></td>
							<td align="center"><?php echo $kk = get_count_kk(array('ID_RT' => $row->ID))?></td>
							<td align="center"><?php echo $in = get_count_individu(array('ID_RT' => $row->ID))?></td>
							<td align="center">
								<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-rw" onclick="getDataRwForEdit('')" ><i class="fa fa-edit"></i> Edit</button>
								<?php if( $kk > 0 || $in > 0){ ?>
									<button href="" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement='top' title='Pastikan tidak ada RT / KK / Individu di RW <?php echo $row->NOMOR_RT;?>' ><i class="fa fa-trash"></i> Hapus</button>
								<?php } else { ?>
									<button href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-user" onclick="deleteRw('<?php echo encode($row->ID); ?>')"  ><i class="fa fa-trash"></i> Hapus</button>
								<?php } ?>
							</td>
						</tr>
						<?php $no++; } ?>
					</table>
					<?php } else {
						if($cari)
							echo "<br/><center><i class='fa fa-exclamation-triangle fa-3x' style='margin-top:10px;'></i> <br/>Tidak ada nomor RT dg kata kunci <b>$cari</b></center><br/>";
						else
							echo "<br/><center><i class='fa fa-exclamation-triangle fa-3x' style='margin-top:10px;'></i> <br/>No Data Availabel</b></center><br/>";
				}?>
				<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
			</div>
		</div>
	</div>
</div>
<div class='row'>
	<div class='col-sm-4 col-xs-12' style='margin-top:10px;margin-bottom:10px'>
		Total <?php echo $paging['count_row']?> Rows 
	</div>
	<div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
		<?php echo $paging['list'] ?>
	</div>
</div>
