<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Individu extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		
	}

    public function index() {
		
        $data['title'] = 'Individu';
        $data['pg'] = 'individu';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/layout/vwContent');
        $this->load->view('back/layout/vwFooter');
    }
}