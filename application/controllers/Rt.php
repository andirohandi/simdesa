<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rt extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		$this->load->model(array('m_dusun','m_rt'));
		$this->load->helper('general_helper');
		$this->load->library('form_validation');
		
	}

    public function index() {
		
        $data['title'] = 'RT';
        $data['pg'] = 'rt';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/rt/vwIndex');
        $this->load->view('back/layout/vwFooter');
    }
	
	function read($pg=1){
		
		$cari 	= trim($this->input->post('cari'));
        $limit 	= trim($this->input->post('limit'));
		$offset = ($limit*$pg)-$limit;
		$like	= '';
		
		if($cari) 
			$like = "(NOMOR_RT like '%$cari%' )";

        $page = array();
        $page['limit'] = $limit;
        $page['count_row'] = $this->m_rt->getCountRt($like);
        $page['current'] = $pg;
        $page['list'] = gen_paging($page);

        $data['paging'] = $page;
		$data['cari']	= $cari;
        $data['data'] = $this->m_rt->getDataRt($like, $limit, $offset);
		
		$this->load->view('back/rt/vwListRw',$data);
	}
	
	function tambah_rt(){
		if(!isset($_POST['simpan_rt'])){
			$this->load->view('er_503');
		}else{
			
			$this->form_validation->set_rules('dusun_simpan','Nama Dusun','trim|required');
			$this->form_validation->set_rules('nomor_rw_simpan','Nomor RW','trim|required');
			$this->form_validation->set_rules('nomor_rt_simpan','Nomor RT','trim|required');
			//$this->form_validation->set_rules('alamat_simpan','Nomor RT','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
				);
				$this->session->set_flashdata($hasil);
				redirect('rt','refresh');
			}else{
				
				$dusun		= $this->input->post('dusun_simpan',true);
				$no_rw		= $this->input->post('nomor_rw_simpan',true);
				$no_rt		= $this->input->post('nomor_rt_simpan',true);
				$ketua_rt	= $this->input->post('ketua_simpan',true);
				$alamat_rt	= $this->input->post('alamat_simpan',true);
				$hasil		= '';
				
				$data = array(
					'ID'			=> '',
					'NOMOR_RT'		=> $no_rt,
					'KETUA_RT'	=> $ketua_rt,
					'ALAMAT_RT'	=> $alamat_rt,
					'ID_DUSUN'	=> $dusun,
					'ID_RW'	=> $no_rw
				);
				
				$query = $this->m_rt->getInsertRt($data);
				
				if(!$query){
					$hasil = array(
						'hasil' => 2,
						'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
					);
				}else{
					$hasil = array(
						'hasil' => 1,
						'msg'	=> '<b>Data berhasil disimpan</b>'
					);
				}
				
				$this->session->set_flashdata($hasil);
				redirect('rt','refresh');
			}
		}
	}
	
	function cek_nomor_rt(){
		$rw		= trim($this->input->post('rw',true));
		$dusun	= trim($this->input->post('dusun',true));
		$rt		= trim($this->input->post('rt',true));
		$hasil	= '';
		
		$data = array(
			'NOMOR_RT'	=> $rt,
			'ID_DUSUN'	=> $dusun,
			'ID_RW'	=> $rw
		);
		
		$query	= $this->m_rt->getCekNomorRt($data);
		
		if($query->num_rows() == 0){
			$hasil = array(
				'rs'	=> 1
			);
		}else{
			$hasil = array(
				'rs'	=> 2
			);
		}
		
		echo json_encode($hasil);
	}
}