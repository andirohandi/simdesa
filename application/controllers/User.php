<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		$this->load->model('m_user');
		$this->load->helper('general_helper');
		$this->load->library('form_validation');
	}

    public function index() {
		
        $data['title'] = 'User';
        $data['pg'] = 'user';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/user/vwIndex');
        $this->load->view('back/layout/vwFooter');
    }
	
	// ganti password by session id
	function ganti_password(){
		if(!isset($_POST['simpan'])){
			$this->load->view('er_503');
		}else{
			
			
			$password		= trim($this->input->post('password_baru',true));
			$id_user_pass	= trim($this->input->post('id_user_pass',true));
			$url			= trim($this->input->post('url_pass',true));
			$hasil			= '';
			
			$data	= array(
				'PASSWORD'	=> md5("hthxdhjklapJIOjh".md5($password)."asdfcnXhyYhgsdhGXChi")
			);
			
			$query = $this->m_user->getUpdateUser($data,$id_user_pass);

			if(!$query){
				$hasil	= array(
					'ganti_password' => 2
				);
			}else{
				$hasil	= array(
					'ganti_password' => 1
				);
			}
			
			$this->session->set_flashdata($hasil);
			
			redirect($url,'refresh');
		}
	}
	
	function read($pg = 1) {

        $cari 	= trim($this->input->post('cari'));
        $limit 	= trim($this->input->post('limit'));
		$offset = ($limit*$pg)-$limit;
		$like	= '';
		
		if($cari) 
			$like = "(NAMA like '%$cari%' OR USERNAME like '%$cari%')";

        $page = array();
        $page['limit'] = $limit;
        $page['count_row'] = $this->m_user->getCountUser($like);
        $page['current'] = $pg;
        $page['list'] = gen_paging($page);

        $data['paging'] = $page;
		$data['cari']	= $cari;
        $data['data_user'] = $this->m_user->getDataUser($like, $limit, $offset);

        $this->load->view('back/user/vwListUser', $data);
    }
	
	function tambah_user(){
		if(!isset($_POST['simpan_user'])){
			$this->load->view('er_503');
		}else{
			
			$this->form_validation->set_rules('nama_simpan','NAMA','trim|required');
			$this->form_validation->set_rules('username_simpan','Username','trim|required');
			$this->form_validation->set_rules('password_baru_simpan','Password','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
				);
				$this->session->set_flashdata($hasil);
				redirect('user','refresh');
			}else{
				$nama		= trim($this->input->post('nama_simpan',true));
				$username	= trim($this->input->post('username_simpan',true));
				$password	= trim($this->input->post('password_baru_simpan',true));
				$level		= trim($this->input->post('kategori_simpan',true));
				$hasil		= '';
				
				$data	=	array(
					'ID'		=> '',
					'NAMA'		=> $nama,
					'USERNAME'	=> $username,
					'PASSWORD'	=> md5("hthxdhjklapJIOjh".md5($password)."asdfcnXhyYhgsdhGXChi"),
					'LEVEL'		=> $level
				);
				
				$query = $this->m_user->getInsertUser($data);
				
				if(!$query){
					$hasil = array(
						'hasil' => 2,
						'msg'	=> '<b>Data Gagal disimpan. Periksa kembali inputan anda</b>'
					);
				}else{
					$hasil = array(
						'hasil' => 1,
						'msg'	=> '<b>Data berhasil disimpan</b>'
					);
					
				}
				$this->session->set_flashdata($hasil);
				redirect('user','refresh');
			}
		}
	}
	
	function edit_user(){
		if(!isset($_POST['edit_user'])){
			$this->load->view('er_503');
		}else{
			
			$this->form_validation->set_rules('nama_edit','NAMA','trim|required');
			$this->form_validation->set_rules('username_edit','Username','trim|required');
			$this->form_validation->set_rules('id_edit','ID','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data gagal diubah. Periksa kembali inputan anda</b>'
				);
				$this->session->set_flashdata($hasil);
				redirect('user','refresh');
			}else{
				$nama		= trim($this->input->post('nama_edit',true));
				$id_user	= trim($this->input->post('id_edit',true));
				$username	= trim($this->input->post('username_edit',true));
				$level		= trim($this->input->post('kategori_edit',true));
				$hasil		= '';
				
				$data	=	array(
					'NAMA'		=> $nama,
					'USERNAME'	=> $username,
					'LEVEL'		=> $level
				);
				
				$query = $this->m_user->getUpdateUser($data,$id_user);
				
				if(!$query){
					$hasil = array(
						'hasil' => 2,
						'msg'	=> '<b>Data Gagal diubah. Periksa kembali inputan anda</b>'
					);
				}else{
					$hasil = array(
						'hasil' => 1,
						'msg'	=> '<b>Data berhasil diubah</b>'
					);
					
				}
				$this->session->set_flashdata($hasil);
				redirect('user','refresh');
			}
		}
	}
	
	function delete_user(){
		if(!isset($_POST['delete_user'])){
			$this->load->view('er_503');
		}else{
			$id		= decode(trim($this->input->post('id_delete',true)));
			$hasil	= '';
			$query 	= $this->m_user->getDeleteUserById($id);
			
			if(!$query){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data Gagal dihapus</b>'
				);
			}else{
				$hasil = array(
					'hasil' => 1,
					'msg'	=> '<b>Data berhasil dihapus</b>'
				);
			}
			
			$this->session->set_flashdata($hasil);
			redirect('user','refresh');
		}
	}
}