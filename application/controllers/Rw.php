<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rw extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		$this->load->model(array('m_dusun','m_rw'));
		$this->load->helper('general_helper');
		$this->load->library('form_validation');
		
	}

    public function index() {
		
        $data['title'] = 'RW';
        $data['pg'] = 'rw';

		$this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/rw/vwIndex');
        $this->load->view('back/layout/vwFooter');
    }
	
	function read($pg=1){
		
		$cari 	= trim($this->input->post('cari'));
        $limit 	= trim($this->input->post('limit'));
		$offset = ($limit*$pg)-$limit;
		$like	= '';
		
		if($cari) 
			$like = "(NOMOR_RW like '%$cari%' )";

        $page = array();
        $page['limit'] = $limit;
        $page['count_row'] = $this->m_rw->getCountRw($like);
        $page['current'] = $pg;
        $page['list'] = gen_paging($page);

        $data['paging'] = $page;
		$data['cari']	= $cari;
        $data['data'] = $this->m_rw->getDataRw($like, $limit, $offset);
		
		$this->load->view('back/rw/vwListRw',$data);
	}
	
	function cek_nomor_rw(){
		$rw		= trim($this->input->post('rw',true));
		$dusun	= trim($this->input->post('dusun',true));
		$hasil	= '';
		
		$data = array(
			'NOMOR_RW'	=> $rw,
			'ID_DUSUN'	=> $dusun
		);
		
		$query	= $this->m_rw->getCekNomorRw($data);
		
		if($query->num_rows() == 0){
			$hasil = array(
				'rs'	=> 1
			);
		}else{
			$hasil = array(
				'rs'	=> 2
			);
		}
		
		echo json_encode($hasil);
	}
	
	function tambah_rw(){
		if(!isset($_POST['simpan_rw'])){
			$this->load->view('er_503');
		}else{
			
			$this->form_validation->set_rules('dusun_simpan','Nama Dusun','trim|required');
			$this->form_validation->set_rules('nomor_simpan','Nomor RW','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
				);
				$this->session->set_flashdata($hasil);
				redirect('rw','refresh');
			}else{
				
				$dusun		= $this->input->post('dusun_simpan',true);
				$no_rw		= $this->input->post('nomor_simpan',true);
				$ketua_rw	= $this->input->post('ketua_simpan',true);
				$alamat_rw	= $this->input->post('alamat_simpan',true);
				$hasil		= '';
				
				$data = array(
					'ID'			=> '',
					'NOMOR_RW'		=> $no_rw,
					'KETUA_RW'	=> $ketua_rw,
					'ALAMAT_RW'	=> $alamat_rw,
					'ID_DUSUN'	=> $dusun
				);
				
				$query = $this->m_rw->getInsertRw($data);
				
				if(!$query){
					$hasil = array(
						'hasil' => 2,
						'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
					);
				}else{
					$hasil = array(
						'hasil' => 1,
						'msg'	=> '<b>Data berhasil disimpan</b>'
					);
				}
				
				$this->session->set_flashdata($hasil);
				redirect('rw','refresh');
			}
		}
	}
	
	function edit_rw(){
		
		$this->form_validation->set_rules('nomor','Nama Dusun','trim|required');
		$this->form_validation->set_rules('id','Id','trim|required');
		$this->form_validation->set_rules('dusun','Id','trim|required');
		$hasil		= '';
		
		if($this->form_validation->run() == FALSE){
			$hasil = array(
				'rs' => 2,
				'msg'	=> '<b>Data gagal diubah. Periksa kembali inputan anda</b>'
			);
		}else{
			
			$dusun		= trim($this->input->post('dusun',true));
			$no_rw		= trim($this->input->post('nomor',true));
			$ketua_rw	= trim($this->input->post('ketua',true));
			$alamat_rw	= trim($this->input->post('alamat',true));
			$id			= decode(trim($this->input->post('id',true)));
			$hasil		= '';
			
			$data = array(
				'NOMOR_RW'		=> $no_rw,
				'KETUA_RW'	=> $ketua_rw,
				'ALAMAT_RW'	=> $alamat_rw,
				'ID_DUSUN'	=> $dusun
			);
			
			$query = $this->m_rw->getUpdateRw($data,$id);
		
			if(!$query){
				$hasil = array(
					'rs' => 2,
					'msg'	=> '<b>Data gagal diubah. Periksa kembali inputan anda</b>'
				);
			}else{
				$hasil = array(
					'rs' => 1,
					'msg'	=> '<b>Data berhasil diubah</b>'
				);
			}
			
			echo json_encode($hasil);
		}
	}
	
	function delete_rw(){
		$id		= decode(trim($this->input->post('x',true)));
		
		$query  = $this->m_rw->getDeleteRwById($id);
		$hasil	= '';
		
		if(!$query){
			$hasil = array('rs'=>2);
		}else{
			$hasil = array('rs'=>1);
		}
		
		echo json_encode($hasil);
	}
	
	function get_select_rw($id_dusun=''){
		$hasil	= '';
		$hasil	.= "<select class='form-control' id='nomor_rw_simpan' name='nomor_rw_simpan' required onchange='cekNomorRt()' id='select-rw' >";
		if($id_dusun==''){
			$hasil 	.= "<option value=''>-- PILIH RW --</option>";
		}else{
			$hasil 	.= "<option value=''>-- PILIH RW --</option>";
			
			$query	= $this->m_rw->getDataRw($where=array('ID_DUSUN'=>$id_dusun));
			
			foreach($query->result() as $row){
				$hasil 	.= "<option value='$row->ID'>$row->NOMOR_RW</option>";
			}
			
		}
		$hasil .= "</select>";
		echo $hasil;
	}
}