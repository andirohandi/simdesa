<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		$this->load->model(array('m_dusun','m_rw'));
		$this->load->helper('general_helper');
		$this->load->library('form_validation');
	}

	function index(){
		echo "<center><h2>404 Page Not FoundM</h2></center>";
	}
	
	function cetak_dusun(){
		
		$like	= '';

        $data['data_dusun'] = $this->m_dusun->getDataDusun($like, $limit='', $offset='');
		
		$html = $this->load->view('back/dusun/vwReportDusun',$data,true);
		
		include_once APPPATH.'/helpers/mpdf60/mpdf.php';
		$this->mpdf = new mPDF();
		$this->mpdf->AddPage('P',
		'', '', '', '',
		10, // margin_left
		10, // margin right
		3, // margin top
		0, // margin bottom
		0, // margin header
		12,
		'','','','','','','','','','Letter'); 
		
		$this->mpdf->WriteHTML($html); 
		$tgl = date("d_m_Y");
		$pdfFilePath = 'Data_Dusun_'.$tgl.'_'.$seksi.'.pdf';
		$this->mpdf->Output($pdfFilePath, 'I');

	}
	
	function cetak_rw(){
		
		$like	= '';

        $data['data'] = $this->m_rw->getDataRw($like, $limit='', $offset='');
		
		$html = $this->load->view('back/rw/vwReportRw',$data,true);
		
		include_once APPPATH.'/helpers/mpdf60/mpdf.php';
		$this->mpdf = new mPDF();
		$this->mpdf->AddPage('P',
		'', '', '', '',
		10, // margin_left
		10, // margin right
		3, // margin top
		0, // margin bottom
		0, // margin header
		12,
		'','','','','','','','','','Letter'); 
		
		$this->mpdf->WriteHTML($html); 
		$tgl = date("d_m_Y");
		$pdfFilePath = 'Data_Dusun_'.$tgl.'_'.$seksi.'.pdf';
		$this->mpdf->Output($pdfFilePath, 'I');

	}
	
	public function html($id_evaluasi) { //report using HTML
		$this->load->view('report', $data);
	}
}