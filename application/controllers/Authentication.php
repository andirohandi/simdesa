<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function index()
	{
		echo "<h2 style='text-align:center;color:red;font-weight:bold'>503 Forbidden Access</h2>";
	}
	
	function auth(){
		$data['title'] = 'Login';

        $this->load->view('vwLogin', $data);
	}
	
	function ver($id = ''){
		
		$this->load->library('form_validation');
		$this->load->model('m_admin');
		
		$username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($id) {
            echo "<h2 style='text-align:center;color:red;font-weight:bold'>503 Forbidden Access</h2>";
        } else {
            if (isset($_POST['login'])) {
                $this->form_validation->set_rules('username', 'Username', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                if ($this->form_validation->run() == FALSE) {

                    $dt = array(
                        'result' => '2',
                        'username' => $username
                    );
                    $this->session->set_flashdata($dt);
                    $this->index();
                } else {
					$dt	= array(
						'USERNAME' => $username,
						'PASSWORD' => md5("hthxdhjklapJIOjh".md5($password)."asdfcnXhyYhgsdhGXChi"),
					);
					
					
					$cek = $this->m_admin->getSelectAuth($dt);
					
                    if (!$cek) {
                        $this->session->set_flashdata('error', 'Username dan Password anda tidak benar');
                        redirect('authentication/auth', 'refresh');
                    } else {
						
						$login 	= array(
							'ID' => $cek['ID'],
							'USERNAME' => $cek['USERNAME'],
							'NAMA' => $cek['NAMA'],
							'LEVEL' => $cek['LEVEL'],
							'logged_in' => true
						);
						
						$this->session->set_userdata($login);
						redirect('dashboard','refresh');
                    }
                }
            } else {
                echo "<h2 style='text-align:center;color:red;font-weight:bold'>503 Forbidden Access</h2>";
            }
        }
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('authentication/auth','refresh');
    }
}
