<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		
	}

    public function index() {
		
        $data['title'] = 'Pendidikan';
        $data['pg'] = 'pendidikan';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/layout/vwContent');
        $this->load->view('back/layout/vwFooter');
    }
}