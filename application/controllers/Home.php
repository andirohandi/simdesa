<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if($this->session->userdata('logged_in')){
			redirect('dashboard','refresh');
		}
	}

	public function index()
	{
		$data['title']	= "Login";
		$this->load->view('vwLogin',$data);
	}
}
