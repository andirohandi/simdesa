<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dusun extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('home','refresh');
		}
		$this->load->model('m_dusun');
		$this->load->helper('general_helper');
		$this->load->library('form_validation');
	}

    public function index() {
		
        $data['title'] = 'Dusun';
        $data['pg'] = 'dusun';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/dusun/vwIndex');
        $this->load->view('back/layout/vwFooter');
    }
	
	function read($pg=1){
		
		$cari 	= trim($this->input->post('cari'));
        $limit 	= trim($this->input->post('limit'));
		$offset = ($limit*$pg)-$limit;
		$like	= '';
		
		if($cari) 
			$like = "(NAMA_DSN like '%$cari%' )";

        $page = array();
        $page['limit'] = $limit;
        $page['count_row'] = $this->m_dusun->getCountDusun($like);
        $page['current'] = $pg;
        $page['list'] = gen_paging($page);

        $data['paging'] = $page;
		$data['cari']	= $cari;
        $data['data_dusun'] = $this->m_dusun->getDataDusun($like, $limit, $offset);
		
		$this->load->view('back/dusun/vwListDusun',$data);
	}
	
	function tambah_dusun(){
		if(!isset($_POST['simpan_dusun'])){
			$this->load->view('er_503');
		}else{
			
			$this->form_validation->set_rules('nama_simpan','Nama Dusun','trim|required');
			//$this->form_validation->set_rules('ketua_simpan','Nama Ketua Dusun','trim|required');
			//$this->form_validation->set_rules('alamat_simpan','Password','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$hasil = array(
					'hasil' => 2,
					'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
				);
				$this->session->set_flashdata($hasil);
				redirect('dusun','refresh');
			}else{
				
				$nama_dusun	= $this->input->post('nama_simpan',true);
				$ketua_dusun= $this->input->post('ketua_simpan',true);
				$alamat		= $this->input->post('alamat_simpan',true);
				$hasil		= '';
				
				$data = array(
					'ID'			=> '',
					'NAMA_DSN'		=> $nama_dusun,
					'ALAMAT_DSN'	=> $alamat,
					'KETUA_DSN'	=> $ketua_dusun
				);
				
				$query = $this->m_dusun->getInsertDusun($data);
				
				if(!$query){
					$hasil = array(
						'hasil' => 2,
						'msg'	=> '<b>Data gagal disimpan. Periksa kembali inputan anda</b>'
					);
				}else{
					$hasil = array(
						'hasil' => 1,
						'msg'	=> '<b>Data berhasil disimpan</b>'
					);
				}
				
				$this->session->set_flashdata($hasil);
				redirect('dusun','refresh');
			}
		}
	}
	
	function edit_dusun(){
		$this->form_validation->set_rules('nama','Nama Dusun','trim|required');
		//$this->form_validation->set_rules('ketua','Nama Ketua Dusun','trim|required');
		$this->form_validation->set_rules('id','Id','trim|required');
		$hasil		= '';
		
		if($this->form_validation->run() == FALSE){
			$hasil = array(
				'rs' => 2,
				'msg'	=> '<b>Data gagal diubah. Periksa kembali inputan anda</b>'
			);
		}else{
			
			$nama_dusun	= $this->input->post('nama',true);
			$ketua_dusun= $this->input->post('ketua',true);
			$alamat		= $this->input->post('alamat',true);
			$id			= decode($this->input->post('id',true));

			$data = array(
				'NAMA_DSN'		=> $nama_dusun,
				'ALAMAT_DSN'	=> $alamat,
				'KETUA_DSN'	=> $ketua_dusun
			);
			
			$query = $this->m_dusun->getUpdateDusun($data,$id);
			
			if(!$query){
				$hasil = array(
					'rs' => 2,
					'msg'	=> '<b>Data gagal diubah. Periksa kembali inputan anda</b>'
				);
			}else{
				$hasil = array(
					'rs' => 1,
					'msg'	=> '<b>Data berhasil diubah</b>'
				);
			}
		}
		
		echo json_encode($hasil);
	}
	
	function cek_nama_dusun(){
		$nama	= trim($this->input->post('x',true));
		$hasil	= '';
		
		$query	= $this->m_dusun->getCekNamaDusun($nama);
		
		if($query->num_rows() == 0){
			$hasil = array(
				'rs'	=> 1
			);
		}else{
			$hasil = array(
				'rs'	=> 2
			);
		}
		
		echo json_encode($hasil);
	}
	
	function delete_dusun(){
		$id		= decode(trim($this->input->post('x',true)));
		
		$query  = $this->m_dusun->getDeleteDusunrById($id);
		$hasil	= '';
		
		if(!$query){
			$hasil = array('rs'=>2);
		}else{
			$hasil = array('rs'=>1);
		}
		
		echo json_encode($hasil);
	}
}