<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model {

	private $table = "tbl_user";
	private $id = "ID";

	public function getUpdateUser($dt,$id)
	{
		return$this->db->set($dt)->where('ID',$id)->update($this->table);
	}
	
	function getDataUser($where='', $limit='', $offset='') {

		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get($this->table);
		else                                     
			$query = $this->db->get($this->table, $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function getCountUser($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
	function getInsertUser($data){
		return $this->db->set($data)->insert($this->table);
	}
	
	function getDeleteUserById($id){
		return $this->db->where('ID',$id)->delete($this->table);
	}
	//"hthxdhjklapJIOjha2a66ed142c891b4927a88696fb62790asdfcnXhyYhgsdhGXChi"
}