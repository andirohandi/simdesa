<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_individu extends CI_Model {

	private $table = "tbl_orang";
	private $id = "ID";
	
	function getCountIndividuById($data){
		$hasil = $this->db->select("COUNT(ID) as count")->where($data)->get($this->table);
		return $hasil->row()->count;
	}

}