<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rw extends CI_Model {

	private $table = "tbl_rw";
	private $id = "ID";
	
	function getCountRwById($data){
		$hasil = $this->db->select("COUNT(ID) as count")->where($data)->get($this->table);
		return $hasil->row()->count;
	}
	
	function getDataRw($where='', $limit='', $offset='') {

		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get($this->table);
		else                                     
			$query = $this->db->get($this->table, $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function getCekNomorRw($data){
		return $this->db->where($data)->get($this->table);
	}
	
	function getCountRw($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
	function getInsertRw($data){
		return $this->db->set($data)->insert($this->table);
	}
	
	public function getUpdateRw($dt,$id)
	{
		return $this->db->set($dt)->where('ID',$id)->update($this->table);
	}
	
	function getDeleteRwById($id){
		return $this->db->where('ID',$id)->delete($this->table);
	}

}