<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rt extends CI_Model {

	private $table = "tbl_rt";
	private $id = "ID";
	
	function getCountRtById($data){
		$hasil = $this->db->select("COUNT(ID) as count")->where($data)->get($this->table);
		return $hasil->row()->count;
	}
	
	function getDataRt($where='', $limit='', $offset='') {

		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get($this->table);
		else                                     
			$query = $this->db->get($this->table, $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function getCountRt($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
	function getInsertRt($data){
		return $this->db->set($data)->insert($this->table);
	}
	
	function getCekNomorRt($data){
		return $this->db->where($data)->get($this->table);
	}

}