<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kk extends CI_Model {

	private $table = "tbl_kk";
	private $id = "ID";
	
	function getCountKkById($data){
		$hasil = $this->db->select("COUNT(ID) as count")->where($data)->get($this->table);
		return $hasil->row()->count;
	}

}